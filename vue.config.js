const path = require('path');
const webpack = require('webpack')

module.exports = {
  devServer: {
    proxy: {
      '^/nexus': {
        target: 'http://192.168.108.39:30002',
        changeOrigin: true
      },
      '^/jira': {
        target: 'http://192.168.108.39:30002',
        changeOrigin: true
      }
    }
  },
  publicPath: process.env.NODE_ENV === 'production'
    ? process.env.PUBLIC_PATH
    : '/',
  configureWebpack: {
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
      }),
    ],
    resolve: {
      alias: {
        "~": path.resolve(__dirname, 'gitlab/app/assets/javascripts'),
        "@": path.resolve(__dirname, 'src')
      }
    }
  }
}
