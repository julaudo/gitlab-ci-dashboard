import { convertToSnakeCase } from './text_utility';
import isFunction from 'lodash.isfunction'
import isObject from 'lodash.isobject'

/**
 * This function takes a conversion function as the first parameter
 * and applies this function to each prop in the provided object.
 *
 * This method also supports additional params in `options` object
 *
 * @param {ConversionFunction} conversionFunction - Function to apply to each prop of the object.
 * @param {Object} obj - Object to be converted.
 * @param {Object} options - Object containing additional options.
 * @param {boolean} options.deep - FLag to allow deep object converting
 * @param {Array[]} options.dropKeys - List of properties to discard while building new object
 * @param {Array[]} options.ignoreKeyNames - List of properties to leave intact (as snake_case) while building new object
 */
export const convertObjectProps = (conversionFunction, obj = {}, options = {}) => {
  if (!isFunction(conversionFunction) || obj === null) {
    return {};
  }

  const { deep = false, dropKeys = [], ignoreKeyNames = [] } = options;

  const isObjParameterArray = Array.isArray(obj);
  const initialValue = isObjParameterArray ? [] : {};

  return Object.keys(obj).reduce((acc, prop) => {
    const val = obj[prop];

    // Drop properties from new object if
    // there are any mentioned in options
    if (dropKeys.indexOf(prop) > -1) {
      return acc;
    }

    // Skip converting properties in new object
    // if there are any mentioned in options
    if (ignoreKeyNames.indexOf(prop) > -1) {
      acc[prop] = val;
      return acc;
    }

    if (deep && (isObject(val) || Array.isArray(val))) {
      if (isObjParameterArray) {
        acc[prop] = convertObjectProps(conversionFunction, val, options);
      } else {
        acc[conversionFunction(prop)] = convertObjectProps(conversionFunction, val, options);
      }
    } else if (isObjParameterArray) {
      acc[prop] = val;
    } else {
      acc[conversionFunction(prop)] = val;
    }
    return acc;
  }, initialValue);
};

export const convertObjectPropsToSnakeCase = (obj = {}, options = {}) =>
  convertObjectProps(convertToSnakeCase, obj, options);
