import {gql} from 'graphql-request'
import {convertObjectPropsToSnakeCase} from '~/lib/utils/common_utils'
import {graphql} from '@/utils/graphql'
import {get} from '@/utils/axios'
import store from '@/store/store'
import Vue from 'vue'


export async function getPipelines(projectIds, branchName, running, username, updatedAfter, scope) {
  const gitlabURL = store.getters['gitlab/URL']

  const projectSelector = projectIds.map(projectId => `"gid://gitlab/Project/${projectId}"`).join(',')
  let pipelineSelector = ''
  if (running) {
    pipelineSelector = 'status: RUNNING, first: 15'
  } else if (branchName) {
    pipelineSelector = `ref: "${branchName}", first: 1`
  } else if (username) {
    pipelineSelector = `username: "${username}", first: 15, updatedAfter: "${updatedAfter}"`
    if (scope && scope !== 'ALL') {
      pipelineSelector += `, scope: ${scope}`
    }
  }
  const query = gql` {
    projects(ids:[${projectSelector}]) {
      nodes {
        id
        name
        fullPath
        pipelines(${pipelineSelector}) {
          nodes {
            user {
              name
              avatarUrl
              webUrl
            }
            id
            ref
            status
            active
            coverage
            createdAt
            startedAt
            updatedAt
            finishedAt
            duration
            retryable
            stages {
              nodes {
                name
                detailedStatus {
                  detailsPath
                  group
                  hasDetails
                  icon
                  label
                  text
                  tooltip
                },
                groups {
                  nodes {
                    name
                    detailedStatus {
                      detailsPath
                      group
                      hasDetails
                      icon
                      label
                      text
                      tooltip
                      action {
                        buttonTitle
                        icon
                        method
                        path
                        title
                      } 
                    }
                  }
                }
              }
            },
            detailedStatus {
              detailsPath
              group
              hasDetails
              icon
              label
              text
              tooltip
            }
          }
        },
        name
      }
    }
  }`

  let {projects} = await graphql(query)

  const pipelines = []
  projects = convertObjectPropsToSnakeCase(projects, {deep: true})
  const statusOrder = {
    created: 1,
    pending: 2,
    running: 3,
    failed: 4,
    'manual action': 5,
    'waiting for manual action': 6,
    'passed with warnings': 7,
    passed: 8,
    skipped: 9,
    canceled: 10,
  }

  if (projects) {
    projects.nodes.forEach(project => {
      project.id = project.id.match(/\d+/g)[0]
      const projectPipelines = project.pipelines.nodes
      if (projectPipelines.length > 0) {
        for (const pipeline of projectPipelines) {
          pipeline.id = pipeline.id.match(/\d+/g)[0]
          pipeline.status = pipeline.detailed_status
          pipeline.sortableStatus = statusOrder[pipeline.status.label] || 0
          pipeline.status.details_path = gitlabURL + pipeline.status.details_path
          const stages = pipeline.stages.nodes
          pipeline.stages = stages
          if (pipeline.user.avatar_url && pipeline.user.avatar_url.startsWith('/')) {
            pipeline.user.avatar_url = gitlabURL + pipeline.user.avatar_url
          }

          pipeline.details = {
            stages
          }
          stages.forEach(stage => {
            const groups = stage.groups.nodes
            stage.groups = groups
            stage.status = stage.detailed_status
            groups.forEach(group => {
              const status = group.status = group.detailed_status
              if (status.action) {
                status.action.path = status.action.path.replace(/.*\/jobs\/(\d*).*/, `projects/${project.id}/jobs/$1/${status.action.icon}`)
              }
              status.details_path = gitlabURL + status.details_path
              group.jobs = [{
                ...group,
                jobs: null
              }] // Avoid cycle in object
              group.size = 1
            })
          })

          pipelines.push({
            pipeline,
            project
          })
        }
      }
    })
  }
  return pipelines
}

export async function loadMergeRequest(items, ref, referenceBranch) {
  const gitlabURL = store.getters['gitlab/URL']

  const projectSelector = items.filter(item => item.project.default_branch !== ref).map(item => `"gid://gitlab/Project/${item.project.id}"`).join(',')

  const query = gql` {
        projects(ids:[${projectSelector}]) {
          nodes {
            id
            fullPath
            mergeRequests(sourceBranches: "${ref}") {
              nodes {
                iid
                rebaseInProgress
                conflicts
                draft
                targetBranch
                webUrl
                state
                approvedBy {
                  nodes {
                    name
                  }
                }
                reviewers {
                  nodes {
                    id
                    name
                    avatarUrl
                    webUrl
                    username
                  }
                }
                discussions {
                  pageInfo {
                    hasNextPage
                  }
                  nodes {
                    resolved
                    resolvable
                  }
                }
              }
            }
            name
          }
        }
      }`

  const itemsById = items.reduce((result, item) => ({...result, [item.project.id]: item}), {})
  let {projects} = await graphql(query)
  projects = convertObjectPropsToSnakeCase(projects, {deep: true})

  let targetBranch = null
  projects.nodes.forEach(project => {
    if (project.merge_requests.nodes.length > 0) {
      const merge = project.merge_requests.nodes[0]
      targetBranch = merge.target_branch
    }
  })

  await Promise.all(projects.nodes.map(async project => {
    const projectId = project.id.match(/\d+/g)[0]
    const item = itemsById[projectId]
    if (project.merge_requests.nodes.length > 0) {
      const merge = project.merge_requests.nodes[0]
      merge.reviewers = merge.reviewers.nodes
      merge.reviewers.forEach(reviewer => {
        if (reviewer.avatar_url && reviewer.avatar_url.startsWith('/')) {
          reviewer.avatar_url = gitlabURL + reviewer.avatar_url
        }
        reviewer.id = parseInt(reviewer.id.split('/')[4])
      })
      merge.reviewers = merge.reviewers.reduce((a, reviewer) => ({ ...a, [reviewer.id]: reviewer}), {})
      merge.approved = merge.approved_by.nodes.length > 0

      let discussions = merge.discussions
      let resolvable = 0
      let resolved = 0
      if (discussions.page_info.has_next_page) {
        discussions = (await get(`/projects/${projectId}/merge_requests/${merge.iid}/discussions`)).data
        resolvable = discussions.filter(d => d.notes[0].resolvable).length
        resolved = discussions.filter(d => d.notes[0].resolved).length
      } else {
        resolvable = discussions.nodes.filter(d => d.resolvable).length
        resolved = discussions.nodes.filter(d => d.resolved).length
      }
      if (resolvable) {
        merge.discussions = resolved + '/' + resolvable
      } else {
        delete merge.discussions
      }
      Vue.set(item, 'merge', merge)
      item.target_branch = merge.target_branch
    } else {
      item.target_branch = targetBranch || item.project.default_branch
    }
    const target = referenceBranch || item.target_branch
    if (target !== ref && ref !== item.project.default_branch) {
      try {
        const response = await get(`projects/${item.project.id}/repository/compare?from=${target}&to=${ref}`)
        item.commitsList = response.data.commits
        item.commits = response.data.commits ? response.data.commits.length : 0
      } catch {
        item.commits = 0
      }

      try {
        const response = await get(`projects/${item.project.id}/repository/compare?from=${ref}&to=${target}`)
        item.behind = response.data.commits ? response.data.commits.length : 0
      } catch {
        item.behind = 0
      }

    }
  }))
}
