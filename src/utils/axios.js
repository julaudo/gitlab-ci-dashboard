import axios from 'axios'
import store from '../store/store'

const gitlab = (method, url) => {
  const promise = method(url, {
    headers: {
      'Private-Token': store.getters['gitlab/password']
    }
  })
  promise.then(() => {
    if (!store.getters['gitlab/valid']) {
      store.dispatch('gitlab/check')
    }
  }).catch(() => {
    if (store.getters['gitlab/valid']) {
      store.dispatch('gitlab/check')
    }
  })
  return promise
}

export const get = async (url, unique = false, stopCondition = null) => {
  let urlValue = new URL(`${store.getters['gitlab/URL']}/api/v4/${url}`)
  if (unique) {
    urlValue.searchParams.append('per_page', '1')
    urlValue.searchParams.append('page', '0')
    return await gitlab(axios.get, urlValue.toString())
  } else {
    urlValue.searchParams.append('per_page', '100')
  }

  let response = await gitlab(axios.get, urlValue.toString())

  let stop = false
  while (response.headers['x-next-page'] && !stop) {
    urlValue.searchParams.set('page', response.headers['x-next-page'])

    const previous = response.data
    response = await gitlab(axios.get, urlValue.toString())
    stop = stopCondition && stopCondition(response)
    response.data = [...previous, ...response.data]
  }

  return response
}

export const getWeb = (url, group) => {
  return gitlab(axios.get, `${store.getters['gitlab/URL']}/${group.full_path}/${url}`)
}

export const post = (url) => {
  return axios.post(`${store.getters['gitlab/URL']}/api/v4/${url}`, null, {
    headers: {
      'Private-Token': store.getters['gitlab/password']
    }
  })
}

export const put = (url, data) => {
  return axios.put(`${store.getters['gitlab/URL']}/api/v4/${url}`, data, {
    headers: {
      'Private-Token': store.getters['gitlab/password']
    }
  })
}

export const del = (url) => {
  return gitlab(axios.delete, `${store.getters['gitlab/URL']}/api/v4/${url}`)
}

const getAuth = (apiName) => {
  const username = store.getters[`${apiName}/login`]
  const password = store.getters[`${apiName}/password`]
  const basicAuth = 'Basic ' + btoa(username + ':' + password)
  return basicAuth
}

export const nexus = (method, url) => {
  return method(`${store.getters['nexus/URL']}/service/rest/${url}`, {
    headers: {
      'Authorization': getAuth('nexus'),
      'NX-ANTI-CSRF-TOKEN': 'api'
    },
    withCredentials: true
  })
}

export const getNexus = (url) => {
  return nexus(axios.get, url)
}

export const jira = (method, url) => {
  return method(`${store.getters['jira/URL']}/rest/api/2/${url}`, {
    headers: {
      'Authorization': getAuth('jira')
    },
  })
}

export const getJira = (url) => {
  return jira(axios.get, url)
}

export const postNexus = (url, data) => {
  return axios.post(`${store.getters['nexus/URL']}/${url}`, data, {
    headers: {
      'Authorization': getAuth('nexus'),
      'NX-ANTI-CSRF-TOKEN': 'api'
    },
    withCredentials: true
  })
}

export const deleteNexus = (url) => {
  return nexus(axios.delete, url)
}
