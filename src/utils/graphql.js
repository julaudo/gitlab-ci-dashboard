import { GraphQLClient } from 'graphql-request'
import store from '../store/store'

export async function graphql(query) {
  const gitlabURL = store.getters['gitlab/URL']
  const gitlabPassword = store.getters['gitlab/password']

  const graphQLClient = new GraphQLClient(`${gitlabURL}/api/graphql`, {
    headers: {
      authorization: `Bearer ${gitlabPassword}`,
    },
  })
  const promise =  graphQLClient.request(query)
  promise.then(() => {
    if (!store.getters['gitlab/valid']) {
      store.dispatch('gitlab/check')
    }
  }).catch(() => {
    if (store.getters['gitlab/valid']) {
      store.dispatch('gitlab/check')
    }
  })
  return await promise
}
