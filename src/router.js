import Vue from 'vue'
import Router from 'vue-router'
import BranchesPipelines from "./components/BranchesPipelines";
import Settings from "./components/Settings";
import RunningPipelines from "./components/RunningPipelines";
import BranchPipelines from "./components/BranchPipelines";
import store from './store/store'
import Artifacts from "@/components/Artifacts";
import NexusAssets from "@/components/NexusAssets";
import Logs from '@/components/Logs'
import Projects from "./components/Projects";
import TagsPipelines from '@/components/TagsPipelines'
import UserPipelines from '@/components/UserPipelines'
import UsersPipelines from '@/components/UsersPipelines'
import Passthrough from '@/components/Passthrough'
import Setup from '@/components/Setup'
import App from '@/App'
import UsersReviews from '@/components/UsersReviews'
import UserReviews from '@/components/UserReviews'

Vue.use(Router)

const router = new Router({
  mode: 'hash',
  base: '/',
  routes: [
    { path: '*', redirect: '/app' },
    {
      name: 'setup',
      path: '/setup',
      component: Setup,
      meta: {
        title: () => 'Setup'
      }
    },
    {
      path: '/app',
      component: App,
      redirect: {name: 'pipelines:branches'},
      meta: {
        title: () => 'Gitlab CI'
      },
      children: [
        {
          path: 'pipelines',
          name: 'pipelines',
          redirect: {name: 'pipelines:branches'},
          component: Passthrough,
          children: [{
            path: 'branches',
            name: 'pipelines:branches',
            component: BranchesPipelines,
            meta: {
              title: () => null
            },
            children: [{
              path: ':branch',
              name: 'pipelines:branches:branch',
              props: route => ({branchName: route.params.branch}),
              component: BranchPipelines,
              meta: {
                title: (route) => `${route.params.branch} pipelines`
              }
            }],

          }, {
            path: 'tags',
            name: 'pipelines:tags',
            component: TagsPipelines,
            meta: {
              title: () => 'Tags pipelines'
            }
          }, {
            path: 'users',
            name: 'pipelines:users',
            redirect: () => {
              return {name: 'pipelines:users:user', params: {
                  username: store.getters['userPipelines/user']
                }}
            },
            component: UsersPipelines,
            meta: {
              title: (route) => route.params.branch
            },
            children: [{
              path: ':username',
              name: 'pipelines:users:user',
              props: route => ({username: route.params.username}),
              component: UserPipelines,
              meta: {
                title: (route) => `${route.params.username}'s pipelines`
              }
            }],
          }, {
            path: 'reviews',
            name: 'pipelines:reviews',
            redirect: () => {
              return {name: 'pipelines:reviews:user', params: {
                  username: store.getters['userReviews/user']
                }}
            },
            component: UsersReviews,
            meta: {
              title: () => null
            },
            children: [{
              path: ':username',
              name: 'pipelines:reviews:user',
              props: route => ({username: route.params.username}),
              component: UserReviews,
              meta: {
                title: (route) => `${route.params.username}'s reviews`
              }
            }],
          }, {
            path: 'running',
            name: 'pipelines:running',
            component: RunningPipelines,
            meta: {
              title: () => `Running (${store.getters['runningPipelines/runningPipelines'].length})`
            }
          }
          ],
          meta: {
            title: () => 'Pipelines'
          }
        },
        {
          path: 'artifacts',
          name: 'artifacts',
          component: Artifacts,
          meta: {
            title: () => 'Artifacts'
          }
        },
        {
          path: 'projects',
          name: 'projects',
          component: Projects,
          meta: {
            title: () => 'Projects'
          }
        },
        {
          path: 'docker-images',
          name: 'docker-images',
          component: NexusAssets,
          props: {
            format: 'docker'
          },
          meta: {
            title: () => 'Docker images'
          }
        },
        {
          path: 'npm-assets',
          name: 'npm-assets',
          component: NexusAssets,
          props: {
            format: 'npm'
          },
          meta: {
            title: () => 'npm assets'
          }
        },
        {
          path: 'logs',
          name: 'logs',
          component: Logs,
          meta: {
            title: () => 'Logs'
          }
        },
        {
          path: 'settings',
          name: 'settings',
          component: Settings,
          meta: {
            title: () => 'Settings'
          }
        }
      ]
    }
  ]
})

router.beforeEach(async (to, from, next) => {
  next()
  if (to.name !== 'setup' && !store.getters['loaders/fetched']) {
    await store.dispatch('loaders/setLoadingApp', true)
    await store.dispatch('loaders/setFetched', true)
    if (!store.getters['gitlab/groups']) {
      await store.dispatch('loaders/setLoadingMessage', 'Loading groups...')
      await store.dispatch('gitlab/fetchGroups')
    }

    if (!store.getters['gitlab/projects']) {
      await store.dispatch('loaders/setLoadingMessage', 'Loading projects branches...')
      await store.dispatch('gitlab/fetchProjects')
    } else {
      await store.dispatch('gitlab/fetchBranches')
    }

    await store.dispatch('loaders/setLoadingMessage', 'Loading users...')

    if (!store.getters['gitlab/allUsers']) {
      await store.dispatch('gitlab/fetchAllUsers')
    } 
    await store.dispatch('loaders/setLoadingApp', false)
    await store.dispatch('jira/cleanIssues', store.getters['pipelines/branches'])
    await store.dispatch('pipelines/clear', [])
  }
})

router.afterEach((to) => {
  // Use next tick to handle router history correctly
  // see: https://github.com/vuejs/vue-router/issues/914#issuecomment-384477609
  Vue.nextTick(() => {
    document.title = `Gitlab CI - ${decodeURIComponent(to.meta.title(to))}`
  });
});

export default router
