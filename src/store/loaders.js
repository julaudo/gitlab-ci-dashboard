export default {
    namespaced: true,

    state: {
        loadingApp: true,
        loadingMessage: '',
        loadingGroups: false,
        loadingProjects: false,
        loadingRunningPipelines: false,
        fetched: false
    },

    getters: {
        loadingApp: state => state.loadingApp,
        loadingMessage: state => state.loadingMessage,
        loadingGroups: state => state.loadingGroups,
        loadingProjects: state => state.loadingProjects,
        loadingRunningPipelines: state => state.loadingRunningPipelines,
        loadingUserPipelines: state => state.loadingUserPipelines,
        fetched: state => state.fetched
    },

    actions: {
        setFetched({ commit }, fetched) {
            commit('setFetched', fetched)
        },
        setLoadingApp({ commit }, loadingApp) {
            commit('setLoadingApp', loadingApp)
        },
        setLoadingMessage({ commit }, loadingMessage) {
            commit('setLoadingMessage', loadingMessage)
        },
        setLoadingProjects({ commit }, loadingProjects) {
            commit('setLoadingProjects', loadingProjects)
        },
        setLoadingGroups({ commit }, loadingGroups) {
            commit('setLoadingGroups', loadingGroups)
        },
        setLoadingRunningPipelines({ commit }, loadingRunningPipelines) {
            commit('setLoadingRunningPipelines', loadingRunningPipelines)
        },
        setLoadingUserPipelines({ commit }, loadingUserPipelines) {
            commit('setLoadingUserPipelines', loadingUserPipelines)
        },
    },
    mutations: {
        setFetched(state, fetched) {
            state.fetched = fetched
        },
        setLoadingApp(state, loadingApp) {
            state.loadingApp = loadingApp
        },
        setLoadingMessage(state, loadingMessage) {
            state.loadingMessage = loadingMessage
        },
        setLoadingProjects(state, loadingProjects) {
            state.loadingProjects = loadingProjects
        },
        setLoadingGroups(state, loadingGroups) {
            state.loadingGroups = loadingGroups
        },
        setLoadingRunningPipelines(state, loadingRunningPipelines) {
            state.loadingRunningPipelines = loadingRunningPipelines
        },
        setLoadingUserPipelines(state, loadingUserPipelines) {
            state.loadingUserPipelines = loadingUserPipelines
        },
    }
}
