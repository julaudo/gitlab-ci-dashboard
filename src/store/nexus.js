import api from "@/store/api";
import {getNexus, postNexus} from '@/utils/axios'

export default {
    namespaced: true,
    state: {
        ...api.state,
        repositories: [],
        availableSpaceInBytes: 0,
        totalSizeInBytes: 0,
    },
    getters: {
        ...api.getters,
        availableSpaceInBytes: state => state.availableSpaceInBytes,
        totalSizeInBytes: state => state.totalSizeInBytes,
        repositories: state => state.repositories,
        dockerRepository: state => state.dockerRepository
    },
    actions: {
        ...api.actions,
        async validate({ state, commit, dispatch }) {
            try {
                const data = (await getNexus(`internal/ui/user`)).data
                if (data.userId == state.login) {
                    commit('setCurrentUser', data)
                    await dispatch('fetchRepositories')
                } else {
                    commit('setCurrentUser', null)
                    commit('setRepositories', [])
                }
            } catch {
                commit('setCurrentUser', null)
                commit('setRepositories', [])
            }
        },
        async fetchQuotas({commit}) {
            const blobstores = (await getNexus('v1/blobstores')).data
            const availableSpaceInBytes = blobstores.reduce((sum, current) => sum + current.availableSpaceInBytes, 0)
            const totalSizeInBytes = blobstores.reduce((sum, current) => sum + current.totalSizeInBytes, 0)
            commit('setQuota', {
                availableSpaceInBytes,
                totalSizeInBytes
            })
        },
        async rebuildIndex({state}) {
            await postNexus(`repositories/${state.repository.name}/rebuild-index`)
        },
        async fetchRepositories({commit}) {
            const repositories = (await getNexus('v1/repositories'))
                .data
                .sort((a, b) => {
                    return a.name.localeCompare(b.name)
                })
            commit('setRepositories', repositories)
        },
        async setDockerRepository({commit}, dockerRepository) {
            commit('setDockerRepository', dockerRepository)
        }
    },
    mutations: {
        ...api.mutations,
        setQuota(state, quota) {
            state.availableSpaceInBytes = quota.availableSpaceInBytes
            state.totalSizeInBytes = quota.totalSizeInBytes
        },
        setRepositories(state, repositories) {
            state.repositories = repositories
        },
        setDockerRepository(state, dockerRepository) {
            state.dockerRepository = dockerRepository
        }
    }

}
