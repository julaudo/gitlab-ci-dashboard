import api from "@/store/api";
import { get } from "@/utils/axios";
import { sortBy, uniq, indexBy } from 'underscore'
import Vue from 'vue'


export default {
  namespaced: true,

  state: {
    ...api.state,
    groups: [],
    selectedGroups: [],
    projects: [],
    ignoredProjects: [],
    branches: {}, // branch_name => {project_id: branch}
    referenceBranches: {}, // branch_name => reference_branch
    tags: [],
    users: {},
    recentUsers: {},
    eventsBranches: [],
    allUsers: [],
    allUsersByName: {},
    allUsersByUsername: {}
  },

  getters: {
    ...api.getters,
    referenceBranch: state => branch => state.referenceBranches[branch],
    groups: state => state.groups,
    selectedGroups: state => state.selectedGroups,
    projects: state => state.projects,
    branches: state => state.branches,
    tags: state => state.tags,
    ignoredProjects: state => state.ignoredProjects,
    selectedProjects: state => {
      const ignoredProjectsIds = state.ignoredProjects.map(project => project.id)
      return state.projects.filter(project => !ignoredProjectsIds.includes(project.id))
    },
    recentUsers: state => {
      let counts = Object.keys(state.recentUsers).map(id => ({id, count: state.recentUsers[id]}))
      return sortBy(counts, 'count').map(user => state.users[user.id]).slice(0, 5)
    },
    eventsBranches: state => state.eventsBranches,
    allUsers: state => state.allUsers,
    getUser: state => (
      (name) =>
        state.allUsersByName[name] || state.allUsersByUsername[name]
    )
  },

  actions: {
    ...api.actions,
    setReferenceBranch({ commit }, { branch, referenceBranch} ) {
      commit('setReferenceBranch', { branch, referenceBranch})
    },
    async fetchAllUsers({ commit }) {
      const allUsers = (await get('users?active=true')).data
      commit('setAllUsers', allUsers)
    },
    async check({ commit, state }) {
      let user = null
      if (state.URL && state.password) {
        try {
          user = (await get('user')).data
        } catch {
          // auth error, keep user=null
        }
      }
      commit('setCurrentUser', user)
    },
    async validate({ commit, dispatch, state }) {
      try {
        let user = null
        if (state.URL && state.password) {
          try {
            user = (await get('user')).data
          } catch {
            // auth error, keep user=null
          }
        }
        commit('setCurrentUser', user)
        if (user) {
          commit('loaders/setLoadingMessage', 'Loading groups...', {root:true})
        }
        if (!user) {
          commit('setGroups', [])
          commit('selectGroups', [])
          commit('setEvents', [])
          return
        }
        await dispatch('fetchGroups')
        if (!state.selectedGroups.length) {
          commit('loaders/setLoadingMessage', 'Loading user history...', {root:true})
          const events = (await get('events', null, (response) => {
            return response.headers['x-page'] > 2
          })).data

          const groupsIds = new Set()
          const project_ids = uniq(events.map(event => event.project_id))
          commit('loaders/setLoadingMessage', 'Loading projects...', {root:true})

          await Promise.all(project_ids.map(async projectId => {
            let url = `projects/${projectId}`
            const project = (await get(url, true)).data
            groupsIds.add(project.namespace.id)
          }))
          const selectedGroups = state.groups.filter(group => groupsIds.has(group.id))
          await dispatch('selectGroups', selectedGroups)

          commit('setEvents', events)
        }
      } finally {
        commit('loaders/setLoadingMessage', null, {root:true})
      }
    },
    async selectGroups({ commit, dispatch }, groups) {
      commit('selectGroups', groups)
      await dispatch('fetchProjects')
    },
    async fetchGroups({ commit, dispatch }) {
      dispatch('loaders/setLoadingGroups', true, {root:true})
      try {
        const groups = (await get('groups')).data
        commit('setGroups', groups)
      } finally {
        dispatch('loaders/setLoadingGroups', false, {root:true})
      }
    },
    async setIgnoredProjects({ commit }, ignoredProjects) {
      commit('setIgnoredProjects', ignoredProjects)
    },
    async deleteProjectBranch({ commit }, {project, branchName}) {
      commit('deleteProjectBranch', {project, branchName})
    },
    async addProjectBranch({ commit }, {project, branchName}) {
      const branch = (await get(`projects/${project.id}/repository/branches/${branchName}`)).data
      commit('addProjectBranch', {project, branch})
    },
    async fetchBranches({ commit, getters }) {
      commit('loaders/setLoadingMessage', 'Loading branches...', {root:true})
      const branches = {}
      await Promise.all(getters.selectedProjects.map(async project => {
        let retry = 0
        let projectBranches
        while(retry < 5) {
          try {
            projectBranches = (await get(`projects/${project.id}/repository/branches`)).data
            break
          } catch {
            retry++
          }
        }
        projectBranches.forEach(branch => {
          if (!branches[branch.name]) {
            branches[branch.name] = {
              [project.id]: branch
            }
          } else {
            branches[branch.name][project.id] = branch
          }
        })
      }))

      commit('setBranches', branches)
    },
    async fetchTags({ commit, getters }) {
      const tags = {}
      await Promise.all(getters.selectedProjects.map(async project => {
        const projectTags = (await get(`projects/${project.id}/repository/tags`)).data
        projectTags.forEach(tag => {
          if (!tags[tag.name]) {
            tags[tag.name] = [project.name]
          } else {
            tags[tag.name].push(project.name)
          }
        })
      }))
      commit('setTags', tags)
    },
    async fetchProjects({ commit, dispatch, state }) {
      if (!state.currentUser) {
        return
      }

      try {
        commit('loaders/setLoadingMessage', 'Loading projects...', {root:true})
        dispatch('loaders/setLoadingProjects', true, {root:true})
        let projects = []
        for(const group of state.selectedGroups) {
          try {
            let groupProjects = (await get('groups/' + group.id)).data.projects
            groupProjects.forEach(p => p.group = group)
            projects = [...projects, ...groupProjects]
          } catch(e) {
            // we should remove group from the list if this is a 404
          }
        }
        commit('setProjects', sortBy(projects, 'name_with_namespace'))
        await dispatch('fetchBranches')
      } finally {
        dispatch('loaders/setLoadingProjects', false, {root:true})
      }
    },
    useUser({ commit }, user) {
      commit('useUser', user)
    }
  },
  mutations: {
    ...api.mutations,
    setReferenceBranch(state, {branch, referenceBranch}) {
      Vue.set(state.referenceBranches, branch, referenceBranch)
    },
    setEvents(state, events) {
      const eventsBranches = new Set()
      const existingBranches = Object.keys(state.branches)
      for (const event of events) {
        if (
          event.push_data
          && event.push_data.ref_type == 'branch'
          && existingBranches.includes(event.push_data.ref)
        ) {
          eventsBranches.add(event.push_data.ref)
        }
      }

      state.eventsBranches = Array.from(eventsBranches)
    },
    setGroups(state, groups) {
      state.groups = sortBy(groups, 'full_name')
    },
    selectGroups(state, selectedGroups) {
      state.selectedGroups = selectedGroups
    },
    setProjects(state, projects) {
      state.projects = projects
    },
    deleteProjectBranch(state, {project, branchName}) {
      delete state.branches[branchName][project.id]
    },
    addProjectBranch(state, {project, branch}) {
      state.branches[branch.name][project.id] = branch
    },
    setBranches(state, branches) {
      state.branches = branches
    },
    setTags(state, tags) {
      state.tags = tags
    },
    setIgnoredProjects(state, ignoredProjects) {
      state.ignoredProjects = ignoredProjects
    },
    useUser(state, user) {
      if (!state.recentUsers[user.id]) {
        Vue.set(state.recentUsers, user.id, 0)
      }
      state.users[user.id] = user
      state.recentUsers[user.id]++
    },
    setAllUsers(state, allUsers) {
      state.allUsers = sortBy(allUsers, 'name')
      state.allUsersByName = indexBy(allUsers, 'name')
      state.allUsersByUsername = indexBy(allUsers, 'username')
    }
  }
}
