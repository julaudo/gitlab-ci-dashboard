import {getPipelines} from '@/utils/pipelines'

export default {
  namespaced: true,

  state: {
    runningPipelines: []
  },

  getters: {
    runningPipelines: state => state.runningPipelines
  },

  actions: {
    async fetch({ dispatch, commit, rootGetters }) {
      try {
        dispatch('loaders/setLoadingRunningPipelines', true, {root:true})

        const selectedProjects = rootGetters['gitlab/selectedProjects']
        const pipelines = await getPipelines(
          selectedProjects.map(project => project.id), null, true
        )

        commit('setRunningPipelines', pipelines)

      } finally {
        dispatch('loaders/setLoadingRunningPipelines', false, {root:true})
      }
    },
  },
  mutations: {
    setRunningPipelines(state, runningPipelines) {
      state.runningPipelines = runningPipelines
    }
  }
}
