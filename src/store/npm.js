export default {
  namespaced: true,

  state: {
    ignoredImages: [],
    repository: null,
    imagePattern: '',
    hideTags: true,
  },

  getters: {
    ignoredImages: state => state.ignoredImages,
    repository: state => state.repository,
    imagePattern: state => state.imagePattern,
    hideTags: state => state.hideTags
  },

  actions: {
    clearIgnoredImages({ commit }) {
      commit('clearIgnoredImages')
    },
    ignoreImage({ commit }, image) {
      commit('ignoreImage', image)
    },
    setRepository({ commit }, repository) {
      commit('setRepository', repository)
    },
    setImagePattern({ commit }, imagePattern) {
      commit('setImagePattern', imagePattern)
    },
    setHideTags({ commit }, hideTags) {
      commit('setHideTags', hideTags)
    },
  },
  mutations: {
    clearIgnoredImages(state) {
      state.ignoredImages = []
    },
    ignoreImage(state, image) {
      state.ignoredImages.push(image)
    },
    setRepository(state, repository) {
      state.repository = repository
    },
    setImagePattern(state, imagePattern) {
      state.imagePattern = imagePattern
    },
    setHideTags(state, hideTags) {
      state.hideTags = hideTags
    },
  }
}
