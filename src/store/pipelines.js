import { without } from 'underscore'

export default {
  namespaced: true,

  state: {
    ignoredProjects: [],
    branches: []
  },

  getters: {
    ignoredProjects: state => state.ignoredProjects,
    branches: state => (without(state.branches, null, undefined))
  },

  actions: {
    setIgnoredProjects({ commit }, ignoredProjects) {
      commit('setIgnoredProjects', ignoredProjects)
    },
    async clear({ commit }) {
      commit('clear')
    },
    setBranches({ commit }, branches) {
      commit('setBranches', branches)
    },
    selectBranch({ commit }, branch) {
      commit('selectBranch', branch)
    },
  },
  mutations: {
    setIgnoredProjects(state, ignoredProjects) {
      state.ignoredProjects = ignoredProjects
    },
    clear(state) {
      delete state.runningPipelines
      delete state.runningProjects
    },
    setBranches(state, branches) {
      state.branches = branches
    },
    selectBranch(state, branch) {
      state.branches.push(branch)
    },
  }
}
