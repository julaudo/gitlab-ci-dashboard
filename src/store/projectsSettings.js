export default {
    namespaced: true,

    state: {
        group: null,
        branch: null
    },

    getters: {
        group: state => state.group,
        branch: state => state.branch
    },

    actions: {
        setGroup({ commit }, group) {
            commit('setGroup', group)
        },
        setBranch({ commit }, branch) {
            commit('setBranch', branch)
        }
    },
    mutations: {
        setGroup(state, group) {
            state.group = group
        },
        setBranch(state, branch) {
            state.branch = branch
        }
    }
}
