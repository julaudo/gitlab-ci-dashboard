import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from 'vuex-persistedstate'
import gitlab from './gitlab'
import docker from './docker'
import pipelines from './pipelines'
import runningPipelines from './runningPipelines'
import display from './display'
import notifications from './notifications'
import loaders from './loaders'
import nexus from './nexus'
import npm from './npm'
import jira from './jira'
import projectsSettings from './projectsSettings'
import userPipelines from './userPipelines'
import userReviews from './userReviews'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  modules: {
    jira,
    nexus,
    gitlab,
    docker,
    pipelines,
    runningPipelines,
    display,
    notifications,
    loaders,
    npm,
    projectsSettings,
    userPipelines,
    userReviews
  },
  plugins: [createPersistedState({
    paths: [
      'nexus', 'gitlab', 'docker', 'pipelines', 'display', 'npm', 'jira', 'projectsSettings',
      'userPipelines', 'userReviews'],
  })]
})
