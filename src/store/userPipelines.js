export default {
  namespaced: true,

  state: {
    user: null,
    days: 1,
    scope: null
  },

  getters: {
    user: (state, getters, rootState, rootGetters) => state.user || rootGetters['gitlab/currentUser'].username,
    days: state => state.days,
    scope: state => state.scope
  },

  actions: {
    setUser({ commit }, user) {
      commit('setUser', user)
    },
    setDays({ commit }, days) {
      commit('setDays', days)
    },
    setScope({ commit }, scope) {
      commit('setScope', scope)
    }
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    },
    setDays(state, days) {
      state.days = days
    },
    setScope(state, scope) {
      state.scope = scope
    }
  }
}
