export default {
  namespaced: true,

  state: {
    URL: '',
    login: '',
    password: '',
    currentUser: false
  },

  getters: {
    URL: state => state.URL,
    login: state => state.login,
    password: state => state.password,
    valid: state => !!state.currentUser,
    currentUser: state => state.currentUser
  },

  actions: {
    async setURL({ commit, dispatch, state }, url) {
      if (state.URL !== url) {
        commit('setURL', url)
        await dispatch('validate')
      }
    },
    async setLogin({commit, dispatch, state}, login) {
      if (state.login !== login) {
        commit('setLogin', login)
        await dispatch('validate')
      }
    },
    async setPassword({commit, dispatch, state}, password) {
      if (state.password !== password) {
        commit('setPassword', password)
        await dispatch('validate')
      }
    }
  },
  mutations: {
    setURL(state, url) {
      state.URL = url
    },
    setLogin(state, login) {
      state.login = login
    },
    setPassword(state, password) {
      state.password = password
    },
    setCurrentUser(state, currentUser) {
      state.currentUser = currentUser
    },
  }
}
