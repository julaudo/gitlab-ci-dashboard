export default {
  namespaced: true,

  state: {
    user: null
  },

  getters: {
    user: (state, getters, rootState, rootGetters) => state.user || rootGetters['gitlab/currentUser'].username
  },

  actions: {
    setUser({ commit }, user) {
      commit('setUser', user)
    }
  },
  mutations: {
    setUser(state, user) {
      state.user = user
    }
  }
}
