import Vue from 'vue';
import api from "@/store/api";
import {getJira} from "@/utils/axios";

export default {
    namespaced: true,
    state: {
        ...api.state,
        issues: {}
    },
    getters: {
        ...api.getters,
        issues: state => state.issues
    },
    actions: {
        ...api.actions,
        async validate({ commit }) {
            try {
                const data = (await getJira('myself')).data
                commit('setCurrentUser', data)
            } catch {
                commit('setCurrentUser', null)
            }
        },
        async addIssue({ commit }, issue) {
            commit('addIssue', issue)
        },
        async cleanIssues({ commit, state }, branches) {
            const issues = {}
            for (const branch of branches) {
                const issue = state.issues[branch.toUpperCase()]
                if (issue) {
                    issues[issue.key] = issue
                }
            }
            commit('setIssues', issues)
        }
    },
    mutations: {
        ...api.mutations,
        addIssue(state, issue) {
            Vue.set(state.issues, issue.key, issue)
        },
        setIssues(state, issues) {
            state.issues = issues
        }
    }
}
