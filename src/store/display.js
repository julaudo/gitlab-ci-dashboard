export default {
  namespaced: true,

  state: {
    darkMode: true,
    autoRefresh: true
  },

  getters: {
    darkMode: state => state.darkMode,
    autoRefresh: state => state.autoRefresh
  },

  actions: {
    setDarkMode({ commit }, darkMode) {
      commit('setDarkMode', darkMode)
    },
    setAutoRefresh({ commit }, autoRefresh) {
      commit('setAutoRefresh', autoRefresh)
    }
  },
  mutations: {
    setDarkMode(state, darkMode) {
      state.darkMode = darkMode
    },
    setAutoRefresh(state, autoRefresh) {
      state.autoRefresh = autoRefresh
    }
  }
}
