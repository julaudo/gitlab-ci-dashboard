export default {
  namespaced: true,

  state: {
    errors: []
  },

  getters: {
    errors: state => state.errors,
    count: state => state.errors.filter(e => !e.read).length
  },

  actions: {
    addError({ commit }, error) {
      commit('addError', {
        message: error,
        date: Date().toLocaleString(),
        read: false
      })
    },
    markAsRead({commit}) {
      commit('markAsRead')
    }
  },
  mutations: {
    addError(state, error) {
      state.errors.unshift(error)
    },
    markAsRead(state) {
      state.errors.forEach(error => {
        error.read = true
      })
    }
  }
}
