import Vue from 'vue'
import Passthrough from './components/Passthrough.vue'
import vuetify from './plugins/vuetify'
import moment from 'moment'
import router from './router'
import store from './store/store'
import axios from 'axios'
import {Builder, parseString} from "xml2js"
import {icons, viewBoxes} from "~/vue_shared/components/icon"
import './bootstrap'

var numeral = require("numeral")

Vue.prototype.moment = moment
Vue.config.productionTip = false

Vue.filter("formatNumber", function (value) {
  return numeral(value).format("0.00");
});

const initIcons = async () => {
  const response = await axios.get('icons.svg')
  const builder = new Builder({ headless: true, explicitRoot: false, pretty: false, indent: '' })
  parseString(response.data, (error, result) => {
    result.svg.symbol.forEach(symbol => {
      const id = symbol.$.id
      const viewBox = symbol.$.viewBox
      symbol.$={}
      icons[id] = builder.buildObject(symbol)
        .replace('<root>', '')
        .replace('</root>', '')
        .replace('fill="#FFF"', 'fill="var(--color)"')
      viewBoxes[id] = viewBox
    })
  })
}

const initStore = async() => {
  if (!store.getters['gitlab/URL']) {
    let data = (await axios.get('default_config')).data
    await store.dispatch('gitlab/setURL', data.gitlab.URL)
    await store.dispatch('jira/setURL', 'jira')
    await store.dispatch('nexus/setURL', 'nexus')
  }
}

const init = async () => {
  await initIcons()
  await initStore()

  if (!store.getters['gitlab/valid']) {
    await router.replace({name: 'setup'})
  }
  new Vue({
    vuetify,
    router,
    store,
    render: h => h(Passthrough)
  }).$mount('#app')
}

init()